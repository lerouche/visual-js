(() => {
  const toRGB = hex => /^#([a-f0-9]{2})([a-f0-9]{2})([a-f0-9]{2})$/i.exec(hex)
    .slice(1)
    .map(h => Number.parseInt(h, 16));

  const $form = document.querySelector("#options-form");
  const $observatory = document.querySelector("#observatory");

  const options = {
    get values () {
      return JSON.parse($form.elements.values.value);
    },

    get observerClass () {
      return $form.elements.observerClass.value;
    },
    get entryClass () {
      return $form.elements.entryClass.value;
    },
    get nameClass () {
      return $form.elements.nameClass.value;
    },
    get valueClass () {
      return $form.elements.valueClass.value;
    },

    get intensityDuration () {
      return +$form.elements.intensityDuration.value;
    },
    get minimumGap () {
      return +$form.elements.minimumGap.value;
    },

    get recentlyAddColour () {
      return toRGB($form.elements.recentlyAddColour.value);
    },
    get recentlyGetColour () {
      return toRGB($form.elements.recentlyGetColour.value);
    },
    get recentlySetColour () {
      return toRGB($form.elements.recentlySetColour.value);
    },
    get animationTimeMs () {
      return +$form.elements.animationTimeMs.value;
    },
    get animationTimeFunction () {
      return VisualScript[$form.elements.animationTimeFunction.value];
    },
  };

  let current = null;

  const loadFromOptions = () => {
    if (current) {
      current.destroy();
    }
    current = VisualScript.observe(options.values, {
      bindTo: $observatory,
      observerClass: options.observerClass,
      entryClass: options.entryClass,
      nameClass: options.nameClass,
      valueClass: options.valueClass,

      intensityDuration: options.animationTimeMs,
      minimumGap: options.minimumGap,

      onAdd: ({animate}) => animate(createAnimation(options.recentlyAddColour)),
      onGet: ({intensity, animate}) => animate(createAnimation(options.recentlyGetColour, intensity)),
      onSet: ({intensity, animate}) => animate(createAnimation(options.recentlySetColour, intensity)),
    });
  };

  $form.addEventListener("change", loadFromOptions);
  $form.addEventListener("submit", e => {
    console.log('submit');
    e.preventDefault();
    loadFromOptions();
  });

  const createAnimation = (colour, intensity = 1) => {
    const peakStyle = {
      backgroundRed: colour[0],
      backgroundGreen: colour[1],
      backgroundBlue: colour[2],
      backgroundAlpha: Math.min(1, intensity * 0.2),
    };

    const endStyle = {
      backgroundRed: 0,
      backgroundGreen: 0,
      backgroundBlue: 0,
      backgroundAlpha: 0,
    };

    return {
      stages: [
        [0.33, peakStyle],
        [0.67, peakStyle],
        [1, endStyle],
      ],
      duration: options.animationTimeMs,
      timing: options.animationTimeFunction,
    };
  };


  // setTimeout(function f () {
  //   proxy[Math.random() * proxy.length] = Math.floor(Math.random() * 25);
  //   setTimeout(f, Math.random() * 1000);
  // }, 2000);
})();
