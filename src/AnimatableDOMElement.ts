import {StyleName, Style} from "./Animator";
import {Config} from "./Config";
import {Value} from "./main";

export class AnimatableDOMElement {
  private readonly cfg: Config;
  private readonly $elem: HTMLDivElement;
  readonly style: Style = {
    backgroundRed: 0, backgroundGreen: 0, backgroundBlue: 0, backgroundAlpha: 0,
  };

  constructor (cfg: Config, $parent: AnimatableDOMElement | HTMLElement | null, classes: (string | undefined)[], content?: Value) {
    this.cfg = cfg;
    const $elem = this.$elem = document.createElement("div");
    $elem.className = classes.join(" ");
    if (content != undefined) {
      this.content = content;
    }

    if ($parent) {
      if ($parent instanceof AnimatableDOMElement) {
        $parent.appendChild(this);
      } else {
        $parent.appendChild($elem);
      }
    }
  }

  setStyle (prop: StyleName, value: number) {
    const {style, $elem} = this;

    style[prop] = value;

    switch (prop) {
    case "backgroundRed":
    case "backgroundGreen":
    case "backgroundBlue":
    case "backgroundAlpha":
      $elem.style.backgroundColor = `rgba(${style.backgroundRed}, ${style.backgroundGreen}, ${style.backgroundBlue}, ${style.backgroundAlpha})`;
      break;
    }
  }

  set content (textContent: Value) {
    this.$elem.textContent = `${textContent}`;
  }

  appendChild ($child: AnimatableDOMElement | HTMLElement) {
    if ($child instanceof AnimatableDOMElement) {
      $child = $child.$elem;
    }
    this.$elem.appendChild($child);
  }

  detach() {
    this.$elem.remove();
  }
}
