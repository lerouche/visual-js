import {AnimatableDOMElement} from "./AnimatableDOMElement";
import {AnimationsQueue} from "./AnimationsQueue";
import {Animator, BEIZER, LINEAR} from "./Animator";
import {Config} from "./Config";
import {EntryRepr} from "./EntryRepr";

export type Value = number | string;

export type ObservableArray<T extends Value> = T[];
export type ObservableObject<T extends Value> = {
  [prop: string]: T;
};

const animator = new Animator();

const parseIndex = (name: any): number | undefined => {
  if (typeof name != "number" && typeof name != "string") {
    return;
  }
  const index = Math.floor(+name);
  // This also checks that it's not NaN, +/-Infinity, etc.
  if (index < 0 || !Number.isSafeInteger(index)) {
    return;
  }
  return index;
};

const parseProperty = (name: any): string | undefined => {
  if (typeof name != "number" && typeof name != "string") {
    return;
  }
  return `${name}`;
};

export const observe = <T extends Value, O extends ObservableObject<T> | ObservableArray<T>> (target: O, config: Config): {
  observed: O;
  destroy: () => void;
} => {
  const $observer = new AnimatableDOMElement(config, config.bindTo, [config.observerClass]);

  const queue = new AnimationsQueue(animator, config.minimumGap || 0);

  const entryElems = {};
  for (const [prop, value] of Object.entries(target)) {
    entryElems[prop] = new EntryRepr(config, queue, animator, $observer, prop, value);
  }

  const nameValidator = Array.isArray(target)
    ? parseIndex
    : parseProperty;

  const proxy = new Proxy(target, {
    get (_, prop) {
      const key = nameValidator(prop);
      if (key !== undefined && entryElems[key]) {
        entryElems[key].onGet();
      }
      return target[prop];
    },
    set (_, prop, value) {
      const key = nameValidator(prop);
      if (key !== undefined) {
        const propElem = entryElems[key];
        if (propElem) {
          propElem.onSet(value);
        } else {
          entryElems[key] = new EntryRepr(config, queue, animator, $observer, `${key}`, value);
        }
      }
      target[prop] = value;
      return true;
    },
  });

  return {
    observed: proxy,
    destroy () {
      $observer.detach();
    },
  };
};

(window as any).VisualJS = {observe, BEIZER, LINEAR};
