import {Animation, Animator} from "./Animator";

export class AnimationsQueue {
  private queue: Animation[] = [];
  private processing: boolean = false;

  constructor (
    private readonly animator: Animator,
    private readonly minGap: number,
  ) {
  }

  enqueue (animation: Animation) {
    this.queue.push(animation);
    this.process();
  }

  process () {
    if (this.processing || !this.queue.length) {
      return;
    }
    this.processing = true;
    const args = this.queue.shift()!;
    this.animator.animate(args);
    setTimeout(() => {
      this.processing = false;
      this.process();
    }, this.minGap);
  }
}
