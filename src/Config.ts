import {AddEvent, GetEvent, SetEvent} from "./EntryRepr";

export interface Config {
  readonly bindTo: HTMLElement;

  readonly observerClass?: string;
  readonly entryClass?: string;
  readonly nameClass?: string;
  readonly valueClass?: string;

  readonly intensityDuration?: number;
  readonly minimumGap?: number;

  readonly onAdd?: (event: AddEvent) => void;
  readonly onGet?: (event: GetEvent) => void;
  readonly onSet?: (event: SetEvent<any>) => void;
}
