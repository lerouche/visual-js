import {AnimatableDOMElement} from "./AnimatableDOMElement";

export type Style = {
  // For the background colour.
  backgroundRed: number;
  backgroundGreen: number;
  backgroundBlue: number;
  backgroundAlpha: number;
};

export type StyleName = keyof Style;

export type AnimationStage = [number, Readonly<Style>];

export interface TimingFunction {
  (progress: number): number;
}

export interface Animation {
  readonly $element: AnimatableDOMElement;
  readonly stages: AnimationStage[];
  readonly duration: number;
  readonly timing: TimingFunction;
}

interface AnimationState {
  readonly started: number;
  readonly stages: AnimationStage[];
  readonly duration: number;
  readonly timing: TimingFunction;
  readonly resolve: () => void;
}

// TODO Import from extlib
const bound = (num: number, min: number, max: number) => Math.max(min, Math.min(max, num));

const applyStyle = ($element: AnimatableDOMElement, style: Style): void => {
  for (const [prop, value] of Object.entries(style)) {
    $element.setStyle(prop as StyleName, value);
  }
};

export const BEIZER = (t: number) => t * t * (3 - 2 * t);
export const LINEAR = (t: number) => t;

export class Animator {
  private readonly current = new Map<AnimatableDOMElement, AnimationState>();

  startUpdateLoop = (): void => {
    requestAnimationFrame(this.updateLoop);
  };

  updateLoop = (): void => {
    if (!this.current.size) {
      return;
    }
    const done = [];
    const time = Date.now();
    for (const [$elem, {started, stages, duration, timing, resolve}] of this.current) {
      const maxProgress = stages[stages.length - 1][0];
      const progress = timing(bound((time - started) / duration, 0, 1));
      if (progress <= 0) {
        applyStyle($elem, stages[0][1]);
        continue;
      }
      if (progress >= maxProgress) {
        applyStyle($elem, stages[stages.length - 1][1]);
        setTimeout(resolve, started + duration - time);
        done.push($elem);
        continue;
      }

      // Find the first stage that applies to past the current progress, and use the stage immediately before that.
      // Note that it's not possible for this to be out-of-bounds as the first stage must always apply at zero,
      // and the above checks ensure that progress is never 0 or lower or 1 or higher.
      const currentStageIndex = stages.findIndex(s => s[0] > progress) - 1;
      if (currentStageIndex < 0 || currentStageIndex >= stages.length - 1) {
        throw new Error();
      }
      const fromStage = stages[currentStageIndex];
      const toStage = stages[currentStageIndex + 1];
      const stageProgress = (progress - fromStage[0]) / (toStage[0] - fromStage[0]);
      for (const prop of Object.keys(toStage[1])) {
        const diff = (toStage[1][prop] - fromStage[1][prop]) * stageProgress;
        $elem.setStyle(prop as StyleName, fromStage[1][prop] + diff);
      }
    }
    for (const $elem of done) {
      this.current.delete($elem);
    }
    this.startUpdateLoop();
  };

  animate ({$element, duration, stages, timing}: Animation): Promise<void> {
    return new Promise(resolve => {
      if (stages[0][0] > 0) {
        stages = [[0, $element.style], ...stages];
      }
      this.current.set($element, {
        started: Date.now(),
        stages,
        duration,
        timing,
        resolve,
      });
      this.startUpdateLoop();
    });
  }
}
