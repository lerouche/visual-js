import {AnimationsQueue} from "./AnimationsQueue";
import {AnimatableDOMElement} from "./AnimatableDOMElement";
import {AnimationStage, Animator, TimingFunction} from "./Animator";
import {Config} from "./Config";
import {Value} from "./main";

export interface AnimationOptions {
  stages: AnimationStage[];
  duration: number;
  timing: TimingFunction;
}

interface ChangeEvent {
  // Name of the entry.
  name: string;
  // Function that, when called, animates the entry representation element in the DOM.
  animate: (options: AnimationOptions) => void;
}

export interface AddEvent extends ChangeEvent {
}

export interface GetEvent extends ChangeEvent {
  intensity: number;
}

export interface SetEvent<T extends Value> extends ChangeEvent {
  value: T;
  intensity: number;
}

export class EntryRepr<T extends Value> {
  private readonly $entry: AnimatableDOMElement;
  private readonly $name: AnimatableDOMElement;
  private readonly $value: AnimatableDOMElement;

  private intensity: number = 0;

  constructor (
    private readonly cfg: Config,
    private readonly animationsQueue: AnimationsQueue,
    private readonly animator: Animator,
    $parent: AnimatableDOMElement,
    private readonly name: string,
    value: T,
  ) {
    const $prop = this.$entry = new AnimatableDOMElement(cfg, $parent, [cfg.entryClass]);
    this.$name = new AnimatableDOMElement(cfg, $prop, [cfg.nameClass], name);
    this.$value = new AnimatableDOMElement(cfg, $prop, [cfg.valueClass], value);

    if (cfg.onAdd) {
      cfg.onAdd({name, animate: this.animate});
    }
  }

  private animate = (options: AnimationOptions): void => {
    return this.animationsQueue.enqueue({
      $element: this.$entry,
      duration: options.duration,
      stages: options.stages,
      timing: options.timing,
    });
  };

  private incrementIntensity () {
    if (this.cfg.intensityDuration) {
      this.intensity++;
      setTimeout(() => this.intensity--, this.cfg.intensityDuration);
      return this.intensity;
    } else {
      return 1;
    }
  }

  onGet () {
    if (this.cfg.onGet) {
      const {name, animate} = this;
      this.cfg.onGet({name, animate, intensity: this.incrementIntensity()});
    }
  }

  onSet (value: T) {
    this.$value.content = value;
    if (this.cfg.onSet) {
      this.cfg.onSet({
        name: this.name,
        value: value,
        intensity: this.incrementIntensity(),
        animate: this.animate,
      });
    }
  }
}
